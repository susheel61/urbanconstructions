 <section class="footer-content text-sm-left">
  <div class="container">
	<div class="row">
		<div class="col-sm-6">
			<h1>Engineering your dreams with us</H1>
			<address>
			  URBAN CONSTRUCTIONS <br/>
			  No.- 25/1, MEANEE AVENUE ROAD,<br/>
			  Bangalore- 560042.
			</address>
			<div class="contact-info">
				<dl>
					<dt>Contact 1</dt>
					<dd><a href="callto:+919886234141">+91 9886234141</a></dd><br/>									
					<dt>Contact 2</dt>
					<dd><a href="callto:+918971933214">+91 8971933214</a></dd><br/>									
					<dt>Enquiry</dt>
					<dd><a href="mailto:enquiries@urbanconstructions.co.in">enquiries@urbanconstructions.co.in</a></dd><br/>
					<dt>Information</dt>
					<dd><a href="mailto:info@urbanconstructions.co.in">info@urbanconstructions.co.in</a></dd>
				</dl>
			</div>
		</div>
		<div class="col-sm-6 well-min">
			<h3>LINKS</h3>
			<ul class="marked-list">
				<li><a href="./">Home</a></li>
				<li><a href="aboutus">About US</a></li>
				<li><a href="projects">Projects</a></li>
				<li><a href="gallery">Gallery</a></li>
				<li><a href="contactus">Contact US</a></li>
			</ul>
		</div>				  
	</div>
  </div>
</section>