<div id="header">
	<div class="topHeader-container">
		<div id="topHeader">
			<a href="mailto:info@urbanconstructions.co.in" class="fa-envelope"><span class="glyphicon glyphicon-envelope"></span> info@urbanconstructions.co.in</a>
			<a href="callto:+917799234224" class="fa-phone"><span class="glyphicon glyphicon-earphone"></span>+919886234141</a>
			<ul class="social pull-right hidden-xs hidden-md">
				<li><a target="_blank" title="follow me on facebook" href="#"><img alt="follow me on facebook" src="//login.create.net/images/icons/user/facebook_30x30.png" border=0></a></a></li>
				<li><a target="_blank" title="follow me on twitter" href="#"><img alt="follow me on twitter" src="//login.create.net/images/icons/user/twitter_30x30.png" border=0></a></a></li>
			</ul>
		</div>
	</div>
	<div class="navigation-container">
		<div id="navigation">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="./"><img src="img/logo.png" alt="Engineering your dreams with us"></a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav">
							<li><a href="./">Home</a></li>
							<li><a href="aboutus">About US</a></li>
							<li><a href="projects">Projects</a></li>
							<li><a href="gallery">Gallery</a></li>
							<li><a href="contactus">Contact US</a></li>
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</div>
				<!-- /.container -->
			</nav>
		</div>
	</div>
</div>

<?php 
	function Slider($slideImage,$title) {
		echo '<header id="myCarousel" class="carousel slide">'.
			'<div class="carousel-inner">'.
				'<div class="item active">'.
					'<div class="fill" style="background-image:url(img/'.$slideImage.'.jpg)"></div>'.
					'<div class="carousel-caption">'.
						'<h2>'.$title.'</h2>'.
					'</div>'.
				'</div>'.
			'</div>'.
		'</header>';
	}

?>