<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>About US</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
		<style>
			.page-footer{
				border:none;
				padding:0;
			}
			.carousel-inner h2{
				font-size: 60px;
			}
			.section-content h1{
				padding:0;
				margin:0;
			}
			@media (min-width: 768px){
				.carousel-inner {
					height: 400px;
				}
			}

			@media (max-width: 768px){
				.carousel-inner {
					height: 200px;
				}
				.carousel-inner h2{
					font-size: 30px;
				}
			}
		</style>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <div id="container">
            <?php include 'includes/header.php';?>
            <!-- Full Page Image Background Carousel Header -->
			<?php Slider("slide7","ABOUT US");?>            
            <section class="section-content">
                <div class="container">
                    <div class="row flow-offset-2">
						<div class="col-xs-12">
							<h1>ABOUT US</h1>
							<hr class="short bg-primary">
							<p>Urban Constructions is a Bangalore based and operated construction professional company 
							focused on delivering excellence for our clients on commercial, residential and civil projects 
							throughout the state.</p>
							<p>Urban Constructions service to our clients is not conditional on project size or location.
							Continuous improvement and innovation are hallmarks of Urban Constructions approach to 
							business. Our resources and market position ensure we are exposed to the latest 
							construction techniques, products and materials where we apply them to our projects in the 
							most practical, efficient, commercial and professional manner.</p>
							<p>The quality of our people clearly differentiates Urban Constructions from our competitors. 
							This provides continuity in relationships with our clients and consistency in delivery across 
							the business.</p>
							<p>Urban Constructions continually strives to ensure that our delivery, performance and the 
							quality of work is at the highest standards in the industry.</p>
							<h2>OUR VISION</h2>
							<hr class="short bg-primary">
							<p>To be the leading construction company through delivering excellence and true value to all our clients.</p>
							<table>
								<tr>
									<td>Excellence is achieved through:</td>
									<td>True Value is achieved through:</td>
								</tr>
								<tr>
									<td>
										<ul>
											<li>Solutions, not problems</li>
											<li> Exceeding expectations</li>
											<li>A Professional team and approach</li>
										</ul>
									</td>
									<td>
										<ul>
											<li> Innovation and quality </li>
											<li> Cost and time efficiency </li>
											<li> Long term relationships </li>
										</ul>
									</td>
								</tr>
							</table>
							<h2>OUR VALUES</h2>
							<hr class="short bg-primary">
							<h3>SAFETY</h3>
							<p>Safety First. Safety Always.</p>
							<h3>UNITY</h3>
							<p>We work together as one - internally and externally.</p>
							<h3>RESPECT</h3>
							<p>Integrity and empowerment underpin the way we work.</p>
							<h3>EXCELLENCE</h3>
							<p>We care passionately about delivering quality and value.</p>
						</div>
					</div>
                </div>
            </section>
            <footer class="page-footer">
				<?php include 'includes/bottomfooter.php';?>
            </footer>
        </div>
        <script src="js/vendor/jquery-1.12.3.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $('.carousel').carousel({
            	interval: 5000 //changes the speed
            });
        </script>
    </body>
</html>

