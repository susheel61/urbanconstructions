<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Contact US</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
        <style>
            .page-footer{
            border:none;
            padding:0;
            }
            .carousel-inner h2{
            font-size: 60px;
            }
            .section-content h1{
            padding:0;
            margin:0;
            }
            @media (min-width: 768px){
            .carousel-inner {
            height: 400px;
            }
            }
            @media (max-width: 768px){
            .carousel-inner {
            height: 200px;
            }
            .carousel-inner h2{
            font-size: 30px;
            }
            }
        </style>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <div id="container">
            <?php include 'includes/header.php';?>
            <!-- Full Page Image Background Carousel Header -->
            <?php Slider("slide4","CONTACT US");?> 
            <section class="section-content">
                <div class="container">
                    <div class="row flow-offset-2">
                        <div class="col-xs-12">
                            <form class="well form-horizontal" action="sendmail" method="post"  id="contact_form">
                                <fieldset>
                                    <!-- Form Name -->
                                    <legend>Contact Us Today!</legend>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">First Name</label>  
                                        <div class="col-md-4 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input  name="first_name" placeholder="First Name" class="form-control"  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" >Last Name</label> 
                                        <div class="col-md-4 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input name="last_name" placeholder="Last Name" class="form-control"  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">E-Mail</label>  
                                        <div class="col-md-4 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                                <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Phone #</label>  
                                        <div class="col-md-4 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                                <input name="phone" placeholder="(779)914-1232" class="form-control" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Address</label>  
                                        <div class="col-md-4 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                <input name="address" placeholder="Address" class="form-control" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">City</label>  
                                        <div class="col-md-4 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                <input name="city" placeholder="city" class="form-control"  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">State</label>
                                        <div class="col-md-4 selectContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                                <select name="state" id="selectState" class="form-control selectpicker" >
                                                    <option value="" >Please select your state</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Zip Code</label>  
                                        <div class="col-md-4 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                <input name="zip" placeholder="Zip Code" class="form-control"  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text area -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Project Description</label>
                                        <div class="col-md-4 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                                <textarea class="form-control" name="comment" placeholder="Project Description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Success message -->
                                    <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
                                    <!-- Button -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-warning" >Send <span class="glyphicon glyphicon-send"></span></button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <!-- /.container -->
                </div>
        </div>
        </section>
		<footer class="page-footer">
			<?php include 'includes/topfooter.php';?>
			<?php include 'includes/bottomfooter.php';?>
		</footer>
        </div>
        <script src="js/vendor/jquery-1.12.3.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
		<script src="js/vendor/bootstrap-validator.js"></script>
        <script src="js/validator.js"></script>
		<script src="js/main.js"></script>
        <script>
            $('.carousel').carousel({
            	interval: 5000 //changes the speed
            });
			$.get("https://www.whizapi.com/api/v2/util/ui/in/indian-states-list?project-app-key=cvatbhrnnktkg62ucpmiqvqe", function(response) {
				var jsonResponse = $.parseJSON(response);
				$(jsonResponse.Data).each(function(i,e){
					$("#selectState").append("<option value='"+e.Name+"'>"+e.Name+"</option>");
				});
			});
        </script>
    </body>
</html>