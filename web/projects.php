<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Projects</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
		<style>
			.page-footer{
				border:none;
				padding:0;
			}
			.carousel-inner h2{
				font-size: 60px;
			}
			.section-content h1{
				padding:0;
				margin:0;
			}
			@media (min-width: 768px){
				.carousel-inner {
					height: 400px;
				}
			}

			@media (max-width: 768px){
				.carousel-inner {
					height: 200px;
				}
				.carousel-inner h2{
					font-size: 30px;
				}
			}
		</style>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <div id="container">
			<?php include 'includes/header.php';?>
            <!-- Full Page Image Background Carousel Header -->
            <?php Slider("slide3","PROJECTS");?> 
            <section class="section-content">
                <div class="container">
                    <div class="row flow-offset-2">
						<div class="col-xs-12">
							<h1>PROJECTS</h1>
							<hr class="short bg-primary">
				
						</div>
					</div>
                </div>
            </section>
            <footer class="page-footer">
				<?php include 'includes/bottomfooter.php';?>
            </footer>
        </div>
        <script src="js/vendor/jquery-1.12.3.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $('.carousel').carousel({
            	interval: 5000 //changes the speed
            });
        </script>
    </body>
</html>

