<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Gallery</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/simplelightbox.min.css">
		<style>
			.page-footer{
				border:none;
				padding:0;
			}
			.carousel-inner h2{
				font-size: 60px;
			}
			.section-content h1{
				padding:0;
				margin:0;
			}
			@media (min-width: 768px){
				.carousel-inner {
					height: 400px;
				}
			}

			@media (max-width: 768px){
				.carousel-inner {
					height: 200px;
				}
				.carousel-inner h2{
					font-size: 30px;
				}
			}
			.gallery{
				margin-top:30px;
			}
			.gallery a{
				display: block;
				margin: 10px 0;
				border: 2px solid silver;
				border-radius: 30px;
				overflow: hidden;
			}
			.gallery a img{
				width:100%;
			}
		</style>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <div id="container">
            <?php include 'includes/header.php';?>
            <!-- Full Page Image Background Carousel Header -->
            <?php Slider("slide2","GALLERY");?> 
            <section class="section-content">
                <div class="container">
					<h1>GALLERY</h1>
					<hr class="short bg-primary">
                    <div class="row flow-offset-2 gallery">
						<div class="col-lg-4 col-md-6"><a href="img/image1.jpg"><img src="img/image1.png" alt="" title=""/></a></div>
						<div class="col-lg-4 col-md-6"><a href="img/image2.jpg"><img src="img/image2.png" alt="" title=""/></a></div>
						<div class="col-lg-4 col-md-6"><a href="img/image3.jpg"><img src="img/image3.png" alt="" title=""/></a></div>
						<div class="col-lg-4 col-md-6"><a href="img/image4.jpg"><img src="img/image4.png" alt="" title=""/></a></div>
					</div>
                </div>
            </section>
            <footer class="page-footer">
               	<?php include 'includes/bottomfooter.php';?>
            </footer>
        </div>
        <script src="js/vendor/jquery-1.12.3.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
		<script src="js/simple-lightbox.min.js"></script>
		<script>
            $('.gallery a').simpleLightbox();
        </script>
        <script>
            $('.carousel').carousel({
            	interval: 5000 //changes the speed
            });
        </script>
    </body>
</html>

