$(function(){
	$(".navbar-nav a").removeClass("active");
	$(".navbar-nav a").each(function(i,e){
		var $path = window.location.pathname.substr(1);
		var $href = $(this).attr("href");
		if($path==""){
			$(".navbar-nav a").eq(0).addClass("active");
		}else if($path === $href){
			$(this).addClass("active");
		}
	});	
});